package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class RightIssueListModel implements Serializable {

    @JsonField
    String AllotmentID;
    @JsonField
    String ClientCode;
    @JsonField
    String ScriptCode;
    @JsonField
    String Qty;
    @JsonField
    String sub_broker;
    @JsonField
    String branch_Cd;
    @JsonField
    String REGIOn;
    @JsonField
    String Name;
    @JsonField
    String No;
    @JsonField
    String Symbol;

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getAllotmentID() {
        return AllotmentID;
    }

    public void setAllotmentID(String allotmentID) {
        AllotmentID = allotmentID;
    }

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getScriptCode() {
        return ScriptCode;
    }

    public void setScriptCode(String scriptCode) {
        ScriptCode = scriptCode;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getSub_broker() {
        return sub_broker;
    }

    public void setSub_broker(String sub_broker) {
        this.sub_broker = sub_broker;
    }

    public String getBranch_Cd() {
        return branch_Cd;
    }

    public void setBranch_Cd(String branch_Cd) {
        this.branch_Cd = branch_Cd;
    }

    public String getREGIOn() {
        return REGIOn;
    }

    public void setREGIOn(String REGIOn) {
        this.REGIOn = REGIOn;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNo() {
        return No;
    }

    public void setNo(String no) {
        No = no;
    }
}
