package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class UnderRiskListModel implements Serializable {

    @JsonField
    String RiskID;
    @JsonField
    String ClientCode;
    @JsonField
    String LedgerBalance;
    @JsonField
    String Shortage;
    @JsonField
    String SquareoffValue;
    @JsonField
    String Reason;
    @JsonField
    String SquareoffDate;

    public String getRiskID() {
        return RiskID;
    }

    public void setRiskID(String riskID) {
        RiskID = riskID;
    }

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getLedgerBalance() {
        return LedgerBalance;
    }

    public void setLedgerBalance(String ledgerBalance) {
        LedgerBalance = ledgerBalance;
    }

    public String getShortage() {
        return Shortage;
    }

    public void setShortage(String shortage) {
        Shortage = shortage;
    }

    public String getSquareoffValue() {
        return SquareoffValue;
    }

    public void setSquareoffValue(String squareoffValue) {
        SquareoffValue = squareoffValue;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getSquareoffDate() {
        return SquareoffDate;
    }

    public void setSquareoffDate(String squareoffDate) {
        SquareoffDate = squareoffDate;
    }
}
