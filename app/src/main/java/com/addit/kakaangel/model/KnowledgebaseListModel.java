package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class KnowledgebaseListModel implements Serializable {

    @JsonField
    String KbID;
    @JsonField
    String Kb_titile;
    @JsonField
    String Kb_date;
    @JsonField
    String Kd_link;

    public String getKbID() {
        return KbID;
    }

    public void setKbID(String kbID) {
        KbID = kbID;
    }

    public String getKb_titile() {
        return Kb_titile;
    }

    public void setKb_titile(String kb_titile) {
        Kb_titile = kb_titile;
    }

    public String getKb_date() {
        return Kb_date;
    }

    public void setKb_date(String kb_date) {
        Kb_date = kb_date;
    }

    public String getKd_link() {
        return Kd_link;
    }

    public void setKd_link(String kd_link) {
        Kd_link = kd_link;
    }
}
