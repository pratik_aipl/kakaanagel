package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SecurityHoldingListModel implements Serializable {

    @JsonField
    String ClientStockHoldingID;
    @JsonField
    String ClientCode;
    @JsonField
    String Client_Name;
    @JsonField
    String ScripCode;
    @JsonField
    String Symbol;
    @JsonField
    String NET;
    @JsonField
    String NetValue;

    public String getClientStockHoldingID() {
        return ClientStockHoldingID;
    }

    public void setClientStockHoldingID(String clientStockHoldingID) {
        ClientStockHoldingID = clientStockHoldingID;
    }

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getClient_Name() {
        return Client_Name;
    }

    public void setClient_Name(String client_Name) {
        Client_Name = client_Name;
    }

    public String getScripCode() {
        return ScripCode;
    }

    public void setScripCode(String scripCode) {
        ScripCode = scripCode;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getNET() {
        return NET;
    }

    public void setNET(String NET) {
        this.NET = NET;
    }

    public String getNetValue() {
        return NetValue;
    }

    public void setNetValue(String netValue) {
        NetValue = netValue;
    }
}
