package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class TradingListModel implements Serializable {

    @JsonField
    String BuySellID;
    @JsonField
    String BuySellDate;
    @JsonField
    String ClientCode;
    @JsonField
    String ScripCode;
    @JsonField
    String ScripName;
    @JsonField
    String Symbol;
    @JsonField
    String Ser_Exp;
    @JsonField
    String BuyQty;
    @JsonField
    String BuyPrice;
    @JsonField
    String SellQty;
    @JsonField
    String SellPrice;
    @JsonField
    String MTMGL;
    @JsonField
    String Product_Type;
    @JsonField
    String CurrentMktPrice;
    @JsonField
    String Net_Price;
    @JsonField
    String Strike_Price;
    @JsonField
    String Option_Type;
    @JsonField
    String Net_Val;
    @JsonField
    String NetQty;

    public String getNetQty() {
        return NetQty;
    }

    public void setNetQty(String netQty) {
        NetQty = netQty;
    }

    public String getBuySellID() {
        return BuySellID;
    }

    public void setBuySellID(String buySellID) {
        BuySellID = buySellID;
    }

    public String getBuySellDate() {
        return BuySellDate;
    }

    public void setBuySellDate(String buySellDate) {
        BuySellDate = buySellDate;
    }

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getScripCode() {
        return ScripCode;
    }

    public void setScripCode(String scripCode) {
        ScripCode = scripCode;
    }

    public String getScripName() {
        return ScripName;
    }

    public void setScripName(String scripName) {
        ScripName = scripName;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getSer_Exp() {
        return Ser_Exp;
    }

    public void setSer_Exp(String ser_Exp) {
        Ser_Exp = ser_Exp;
    }

    public String getBuyQty() {
        return BuyQty;
    }

    public void setBuyQty(String buyQty) {
        BuyQty = buyQty;
    }

    public String getBuyPrice() {
        return BuyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        BuyPrice = buyPrice;
    }

    public String getSellQty() {
        return SellQty;
    }

    public void setSellQty(String sellQty) {
        SellQty = sellQty;
    }

    public String getSellPrice() {
        return SellPrice;
    }

    public void setSellPrice(String sellPrice) {
        SellPrice = sellPrice;
    }

    public String getMTMGL() {
        return MTMGL;
    }

    public void setMTMGL(String MTMGL) {
        this.MTMGL = MTMGL;
    }

    public String getProduct_Type() {
        return Product_Type;
    }

    public void setProduct_Type(String product_Type) {
        Product_Type = product_Type;
    }

    public String getCurrentMktPrice() {
        return CurrentMktPrice;
    }

    public void setCurrentMktPrice(String currentMktPrice) {
        CurrentMktPrice = currentMktPrice;
    }

    public String getNet_Price() {
        return Net_Price;
    }

    public void setNet_Price(String net_Price) {
        Net_Price = net_Price;
    }

    public String getStrike_Price() {
        return Strike_Price;
    }

    public void setStrike_Price(String strike_Price) {
        Strike_Price = strike_Price;
    }

    public String getOption_Type() {
        return Option_Type;
    }

    public void setOption_Type(String option_Type) {
        Option_Type = option_Type;
    }

    public String getNet_Val() {
        return Net_Val;
    }

    public void setNet_Val(String net_Val) {
        Net_Val = net_Val;
    }
}
