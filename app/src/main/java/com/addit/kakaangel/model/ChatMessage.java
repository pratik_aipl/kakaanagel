package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class ChatMessage {

    @JsonField
    String ChatID;
    @JsonField
    String Message;
    @JsonField
    String SenderID;
    @JsonField
    String ReceiverID;
    @JsonField
    String SendingDate;
    @JsonField
    String ViewStatus;

    public String getChatID() {
        return ChatID;
    }

    public void setChatID(String chatID) {
        ChatID = chatID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getSendingDate() {
        return SendingDate;
    }

    public void setSendingDate(String sendingDate) {
        SendingDate = sendingDate;
    }

    public String getViewStatus() {
        return ViewStatus;
    }

    public void setViewStatus(String viewStatus) {
        ViewStatus = viewStatus;
    }
}
