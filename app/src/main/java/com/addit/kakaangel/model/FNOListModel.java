package com.addit.kakaangel.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class FNOListModel implements Serializable {

    @JsonField
    String FNOID;
    @JsonField
    String ClientCode;
    @JsonField
    String Segment;
    @JsonField
    String InstType;
    @JsonField
    String ScripName;
    @JsonField
    String Symbol;
    @JsonField
    String ExpiryDate;
    @JsonField
    String OptionType;
    @JsonField
    String StrikePrice;
    @JsonField
    String NetQuantity;

    public String getFNOID() {
        return FNOID;
    }

    public void setFNOID(String FNOID) {
        this.FNOID = FNOID;
    }

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getSegment() {
        return Segment;
    }

    public void setSegment(String segment) {
        Segment = segment;
    }

    public String getInstType() {
        return InstType;
    }

    public void setInstType(String instType) {
        InstType = instType;
    }

    public String getScripName() {
        return ScripName;
    }

    public void setScripName(String scripName) {
        ScripName = scripName;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getOptionType() {
        return OptionType;
    }

    public void setOptionType(String optionType) {
        OptionType = optionType;
    }

    public String getStrikePrice() {
        return StrikePrice;
    }

    public void setStrikePrice(String strikePrice) {
        StrikePrice = strikePrice;
    }

    public String getNetQuantity() {
        return NetQuantity;
    }

    public void setNetQuantity(String netQuantity) {
        NetQuantity = netQuantity;
    }
}
