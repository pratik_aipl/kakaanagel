package com.addit.kakaangel.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.BuildConfig;
import com.addit.kakaangel.R;
import com.addit.kakaangel.model.UserData;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.addit.kakaangel.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_link)
    TextView tv_link;

    Subscription subscription;
    String OTP,MOBILE;
    String PlayerID= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        OneSignal.idsAvailable((userId, registrationId) -> {
            Log.d(TAG, "onCreate: User:" + userId);
            this.PlayerID = userId;
        });
        tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if(BuildConfig.DEBUG){
            edtMobile.setText("A98125");
        }

    }

    @OnClick({R.id.menu_item_one,R.id.btn_login,R.id.tv_link})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_login:
                if (Validation.isEmpty(L.getEditText(edtMobile))) {
                edtMobile.setError("Please enter Valid Code");
                } else {
                    if (L.isNetworkAvailable(LoginActivity.this))
                        GetOTP();
                }
                break;
            case R.id.menu_item_one:
                Intent intent1 = new Intent(Intent.ACTION_DIAL);
                intent1.setData(Uri.parse("tel:9819282125"));
                startActivity(intent1);
                break;
                case R.id.tv_link:
                    Uri uri = Uri.parse("https://nxt.angelbroking.com/SubBrokerAPI/LMSLanding?SBCode=ii8Vo4CH6PU57%2big2Hy8uA%3d%3d");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                break;

        }
    }
    private void GetOTP() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.CLIENTCODE, L.getEditText(edtMobile));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.PlayerID,PlayerID);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOTP(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    OTP=DATA.getString("otp");
                    MOBILE=DATA.getString("mobile_no");

                    Intent intent = new Intent(this, OtpVerificationActivity.class);
                    intent.putExtra(Constant.MOBILE,MOBILE);
                    intent.putExtra(Constant.CLIENTCODE,L.getEditText(edtMobile));
                    intent.putExtra(Constant.OTP,OTP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


}
