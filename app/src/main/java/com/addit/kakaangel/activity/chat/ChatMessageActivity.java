package com.addit.kakaangel.activity.chat;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.listner.DialogButtonListener;
import com.addit.kakaangel.listner.MessageUsers;
import com.addit.kakaangel.listner.RemoveMessage;
import com.addit.kakaangel.model.ChatMessage;
import com.addit.kakaangel.model.ChatUser;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.network.RestApi;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.codesgood.views.JustifiedTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
import static com.addit.kakaangel.utils.ImagePathUtils.getFilePath;

public class ChatMessageActivity extends BaseActivity {

    private static final String TAG = "ChatMessageActivity";
    private static final int REQUEST_IMAGE_CAPTURE = 11;
    private static final int REQUEST_IMAGE_SELECT = 12;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    private int GALLERY = 1, CAMERA = 2;

    @BindView(R.id.img_back)
    ImageView mBack;
    @BindView(R.id.recycler_list)
    RecyclerView rvChat;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.mEmptyList)
    JustifiedTextView mEmptyList;

    Subscription subscriptionMessages, subscriptionSendMessage, subscriptionRemove;
    List<ChatMessage> chatMessageList = new ArrayList<>();
    ChatAdapter chatAdapter;
    @BindView(R.id.mMessageBox)
    EditText mMessageBox;
    @BindView(R.id.mSend)
    ImageView mSend;
    @BindView(R.id.mBottomView)
    LinearLayout mBottomView;
    ChatUser chatUser;
    String userId, userimg, username;
    File imgFile = null, videoFile = null;
    private String mCurrentPhotoPath;
    private Uri mCurrentPhotoPathUri, fileUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_message_activity);
        ButterKnife.bind(this);

        userId = getIntent().getStringExtra(Constant.USERID);
        username = getIntent().getStringExtra(Constant.USERNAME);

      /*  if (user.getRole_ID().equalsIgnoreCase("1")) {
           // chatUser = (ChatUser) getIntent().getSerializableExtra(Constant.USERID);
            userId = chatUser.getId();
            //userimg = chatUser.getProfileImgPath();
            username = chatUser.getClientName();
        } else {
            userId = getIntent().getStringExtra(Constant.USERID);
            username = getIntent().getStringExtra(Constant.USERNAME);
        }*/


        tvTittle.setText(username);

        rvChat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        rvChat.setLayoutManager(linearLayoutManager);
        chatAdapter = new ChatAdapter(this, chatMessageList);
        rvChat.setAdapter(chatAdapter);
        rvChat.scrollToPosition(chatMessageList.size());
        getAllMessage(true);

        swipeLayout.setOnRefreshListener(() -> {
            if (L.isNetworkAvailable(ChatMessageActivity.this)) {
                getAllMessage(false);
            }
        });
    }

    @OnClick({R.id.img_back,R.id.mSend, R.id.mCaptureImage, R.id.mPickImage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

          /*  case R.id.mCaptureImage:
                L.showTwoButtonDialog(ChatMessageActivity.this, "", "Pick", "Image", "Video",  new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        pickIImage(true);
                    }

                    @Override
                    public void onNegativButtonClicked() {
                        pickVideo(true);
                    }
                });

                break;
            case R.id.mPickImage:
                L.showTwoButtonDialog(ChatMessageActivity.this, "", "Pick", "Image", "Video",  new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        pickIImage(false);
                    }

                    @Override
                    public void onNegativButtonClicked() {
                        pickVideo(false);
                    }
                });

                break;*/
            case R.id.mSend:
                if (TextUtils.isEmpty(mMessageBox.getText().toString().trim())) {
                    mMessageBox.setError("Please enter message.");
                    mMessageBox.requestFocus();
                } else {
                    L.newhideKeyboard(ChatMessageActivity.this);
                    sendMessage();
                }
                break;
        }
    }

   /* private void pickIImage(boolean isCamera) {
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        if (isCamera) {
                            final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            startActivityForResult(intent, REQUEST_IMAGE_SELECT);
                        }
                    } else {
                        // Oops permission denied
                    }
                });
    }
*/
  /*  private void pickVideo(boolean isCamera) {
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        if (isCamera) {
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                            startActivityForResult(intent, CAMERA);
                        } else {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            galleryIntent.setType("video/*");
                            startActivityForResult(galleryIntent, GALLERY);
                        }
                    } else {
                        // Oops permission denied
                    }
                });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            imgFile = new File(Objects.requireNonNull(getImagePath()));
            sendMessage();
        } else if (requestCode == REQUEST_IMAGE_SELECT && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            imgFile = new File(Objects.requireNonNull(getFilePath(this, selectedImage)));
            sendMessage();
        } else if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                videoFile = new File(Objects.requireNonNull(getFilePath(this, contentURI)));
                sendMessage();
            }
        } else if (requestCode == CAMERA) {
            sendMessage();
        }
    }

    private void getAllMessage(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put("SenderID", userId);
        showProgress( true);
        subscriptionMessages = NetworkRequest.performAsyncRequest(restApi.getChatList(map), (data) -> {
            showProgress(false);
            swipeLayout.setRefreshing(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {
                        rvChat.setVisibility(View.VISIBLE);
                        mEmptyList.setVisibility(View.GONE);
                        chatMessageList.clear();
                        chatMessageList.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("chat_list").toString(), ChatMessage.class));
                        chatAdapter.notifyDataSetChanged();
                    }else{
                        rvChat.setVisibility(View.GONE);
                        mEmptyList.setVisibility(View.VISIBLE);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            swipeLayout.setRefreshing(false);
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void sendMessage() {
        Map<String, String> map = new HashMap<>();
        map.put("ReceiverID", userId);
        map.put("Message", L.convertStringToUTF8(mMessageBox.getText().toString()));
        Log.d(TAG, "sendMessage: "+map.toString());
        showProgress( true);
        subscriptionMessages = NetworkRequest.performAsyncRequest(restApi.sendMessage(map), (data) -> {
            showProgress(false);
            swipeLayout.setRefreshing(false);
            if (data.code() == 200) {
                try {

                    Log.d(TAG, "sendMessage: "+ data.body());
                    getAllMessage(false);
                    mMessageBox.setText("");
                    Toast.makeText(this, ""+ new JSONObject(data.body()).getString("message"), Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            swipeLayout.setRefreshing(false);
            showProgress(false);
            e.printStackTrace();
        });
    }



    @Override
    protected void onDestroy() {
        if (subscriptionMessages != null && !subscriptionMessages.isUnsubscribed()) {
            subscriptionMessages.unsubscribe();
            subscriptionMessages = null;
        }
        super.onDestroy();
    }


   /* @Override
    public void onRemoveMsg(ChatMessage chatMessage, int pos) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MessageID, chatMessageList.get(pos).getMessageID());
        showProgress(true, true);

        subscriptionRemove = NetworkRequest.performAsyncRequest(restApi.deleteMessage(map), (data) -> {
            showProgress(false, true);
            if (data.code() == 200) {
                chatMessageList.remove(pos);
                chatAdapter.notifyDataSetChanged();
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false, true);
            e.printStackTrace();
        });
    }*/

    public Uri setImageUri() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", getString(R.string.app_name) + Calendar.getInstance().getTimeInMillis() + ".jpg");
            mCurrentPhotoPathUri = Uri.fromFile(file);
            mCurrentPhotoPath = file.getAbsolutePath();
        } else {
            File file = new File(getFilesDir(), getString(R.string.app_name) + Calendar.getInstance().getTimeInMillis() + ".jpg");
            mCurrentPhotoPathUri = Uri.fromFile(file);
            mCurrentPhotoPath = file.getAbsolutePath();
        }
        return mCurrentPhotoPathUri;
    }

    public String getImagePath() {
        return mCurrentPhotoPath;
    }

    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type) {
        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyCameraVideo");
        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(ChatMessageActivity.this, "Failed to create directory MyCameraVideo.",
                        Toast.LENGTH_LONG).show();
                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());


        if (type == MEDIA_TYPE_VIDEO) {
            // For unique video file name appending current timeStamp with file name
            videoFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return videoFile;
    }

}
