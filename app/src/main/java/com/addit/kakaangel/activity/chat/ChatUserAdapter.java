package com.addit.kakaangel.activity.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.addit.kakaangel.R;
import com.addit.kakaangel.listner.DialogButtonListener;
import com.addit.kakaangel.listner.RemoveUser;
import com.addit.kakaangel.model.ChatUser;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChatUserAdapter";

    private int REQUEST_USER = 0;
    private Context mContext;
    private List<ChatUser> chatUsers;
    RemoveUser removeUser;
    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatUserAdapter(ChatActivity context, List<ChatUser> list) {
        mContext = context;
        chatUsers = list;
        removeUser = (RemoveUser) context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_user, parent, false);
        return new ChatUserHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        if (holderIn instanceof ChatUserHolder) {
            ChatUserHolder holder = (ChatUserHolder) holderIn;

         //   L.loadImageWithPicassoRound(chatUsers.get(position).getProfileImgPath(), holder.mUserImage, mContext, 0, 0, null);
            holder.mUserName.setText(chatUsers.get(position).getClientName());
            holder.mView.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, ChatMessageActivity.class)
                    .putExtra(Constant.USERID,chatUsers.get(position).getId())
                    .putExtra(Constant.USERNAME,chatUsers.get(position).getClientName())));

            holder.mDelete.setOnClickListener(v -> L.showTwoButtonDialog(mContext, "", "Are you sure, you want to remove user chat?", "Remove", "No",  new DialogButtonListener() {
                @Override
                public void onPositiveButtonClicked() {
                    removeUser.onRemoveUser(chatUsers.get(position), position);
                }

                @Override
                public void onNegativButtonClicked() {

                }
            }));
        }
    }

    @Override
    public int getItemCount() {
        return chatUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return REQUEST_USER;
    }

    static class ChatUserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mUserImage)
        ImageView mUserImage;
        @BindView(R.id.mDelete)
        ImageView mDelete;
        @BindView(R.id.mUserName)
        TextView mUserName;
        @BindView(R.id.mView)
        CardView mView;
//        @BindView(R.id.mTime)
//        TextView mTime;
//        @BindView(R.id.mMessage)
//        TextView mMessage;

        ChatUserHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
