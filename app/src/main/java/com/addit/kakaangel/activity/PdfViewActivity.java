package com.addit.kakaangel.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.addit.kakaangel.R;
import com.github.barteksc.pdfviewer.PDFView;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PdfViewActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.pdfview)
    PDFView pdfview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);
        ButterKnife.bind(this);
        tvTittle.setText("Rules & Regulation");
        pdfview.fromAsset("dummy.pdf").load();
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}