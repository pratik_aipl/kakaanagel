package com.addit.kakaangel.activity.chat;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;


import com.addit.kakaangel.R;
import com.addit.kakaangel.listner.DialogButtonListener;
import com.addit.kakaangel.listner.MessageUsers;
import com.addit.kakaangel.listner.RemoveMessage;
import com.addit.kakaangel.model.ChatMessage;
import com.addit.kakaangel.utils.L;
import com.addit.kakaangel.utils.TimeAgo;

import java.util.Arrays;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ChatAdapter";
    private static final List<Long> times = Arrays.asList(
            DAYS.toMillis(365),
            DAYS.toMillis(30),
            DAYS.toMillis(7),
            DAYS.toMillis(1),
            HOURS.toMillis(1),
            MINUTES.toMillis(1),
            SECONDS.toMillis(1)
    );
    private final List<String> timesString = Arrays.asList(
            "year", "month", "week", "day", "hour", "minute", "second"
    );
    private int REQUEST_TEXT = 0;
    private int REPLY_TEXT = 1;
    private Context mContext;
    private List<ChatMessage> chatList;


    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatAdapter(ChatMessageActivity context, List<ChatMessage> list) {
        mContext = context;
        chatList = list;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        if (viewType == REQUEST_TEXT) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_request_text, parent, false);
            return new ChatRequestHolder(v);
        } else if (viewType == REPLY_TEXT) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_response_text, parent, false);
            return new ChatReplyHolder(v);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        if (holderIn instanceof ChatRequestHolder) {
            ChatRequestHolder holder = (ChatRequestHolder) holderIn;
            holder.mMessage.setText(L.convertUTF8ToString(chatList.get(position).getMessage().replace("//", " /")));
            holder.mTime.setText(L.changeDateFormat(chatList.get(position).getSendingDate()));
            if (holder.mMessage.getText().length() == 0) {
                holder.mMessage.setVisibility(View.GONE);
            } else {
                holder.mMessage.setVisibility(View.VISIBLE);
            }

            holder.mHours.setText(TimeAgo.getTimeAgo(chatList.get(position).getSendingDate()));//String.format("%s Hours",L.changeFormat(chatList.get(position).getMessageTime())));

        /*    if (chatList.get(position).getMessageImage() != null && !TextUtils.isEmpty(chatList.get(position).getMessageImage())) {
                holder.mImageData.setVisibility(View.VISIBLE);
                L.loadImageWithPicasso(mContext, chatList.get(position).getMessageImage(), holder.mMsgImage, holder.mProgress);
            } else {
                holder.mImageData.setVisibility(View.GONE);
            }*/
         /*   if (chatList.get(position).getMessageVideo() != null && !TextUtils.isEmpty(chatList.get(position).getMessageVideo())) {
                holder.mMessageVideo.setVisibility(View.VISIBLE);
//                holder.mMessageVideo.setVideoPath(chatList.get(position).getMessageVideo()).getPlayer().pause();
                holder.mMessageVideo.setVideoPath(chatList.get(position).getMessageVideo());

            } else {
                holder.mMessageVideo.setVisibility(View.GONE);
            }*/

  /*          holder.mDelete.setOnClickListener(v -> L.showTwoButtonDialog(mContext, "", "Are you sure, you want to remove message?", "Remove", "No",  new DialogButtonListener() {
                @Override
                public void onPositiveButtonClicked() {
                    removeMessage.onRemoveMsg(chatList.get(position), position);
                }

                @Override
                public void onNegativButtonClicked() {

                }
            }));*/


        } else if (holderIn instanceof ChatReplyHolder) {
            ChatReplyHolder holder = (ChatReplyHolder) holderIn;
          //  L.loadImageWithPicassoRound(chatList.get(position).getProfileImgPath(), holder.mUserImage, mContext, 0, 0, null);
           /* holder.mUserImage.setOnClickListener(v -> {
                messageUsers.onUserClick(position, chatList.get(position));
            });*/
            holder.mMessage.setText(L.convertUTF8ToString(chatList.get(position).getMessage().replace("//", " /")));
            if (holder.mMessage.getText().length() == 0) {
                holder.mMessage.setVisibility(View.GONE);
            } else {
                holder.mMessage.setVisibility(View.VISIBLE);
            }
            holder.mTime.setText(L.changeDateFormat(chatList.get(position).getSendingDate()));
            holder.mHours.setText(TimeAgo.getTimeAgo(chatList.get(position).getSendingDate()));

          /*  if (chatList.get(position).getMessageImage() != null && !TextUtils.isEmpty(chatList.get(position).getMessageImage())) {
                holder.mImageData.setVisibility(View.VISIBLE);
                L.loadImageWithPicasso(mContext, chatList.get(position).getMessageImage(), holder.mMsgImage, holder.mProgress);
            } else {
                holder.mImageData.setVisibility(View.GONE);
            }*/
           /* if (chatList.get(position).getMessageVideo() != null && !TextUtils.isEmpty(chatList.get(position).getMessageVideo())) {
                holder.mMessageVideo.setVisibility(View.VISIBLE);
//                holder.mMessageVideo.setVideoPath(chatList.get(position).getMessageVideo()).getPlayer().pause();
                holder.mMessageVideo.setVideoPath(chatList.get(position).getMessageVideo());
            } else {
                holder.mMessageVideo.setVisibility(View.GONE);
            }*/
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatList.get(position) != null) {
            if (chatList.get(position).getSenderID().equalsIgnoreCase(L.getUser(mContext).getId()))
                return REQUEST_TEXT;
            else
                return REPLY_TEXT;
        } else {
            Log.d(TAG, "getItemViewType: " + position);
        }
        return 0;
    }

    static class ChatRequestHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mTime)
        TextView mTime;
        @BindView(R.id.mMessage)
        TextView mMessage;
        @BindView(R.id.mHours)
        TextView mHours;
        @BindView(R.id.mDelete)
        ImageView mDelete;
        @BindView(R.id.mMsgImage)
        ImageView mMsgImage;
        @BindView(R.id.mImageData)
        RelativeLayout mImageData;
        @BindView(R.id.mProgress)
        ProgressBar mProgress;

        ChatRequestHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class ChatReplyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mTime)
        TextView mTime;
        @BindView(R.id.mMessage)
        TextView mMessage;
        @BindView(R.id.mHours)
        TextView mHours;
        @BindView(R.id.mUserImage)
        ImageView mUserImage;
        @BindView(R.id.mMsgImage)
        ImageView mMsgImage;
        @BindView(R.id.mImageData)
        RelativeLayout mImageData;
        @BindView(R.id.mProgress)
        ProgressBar mProgress;

        ChatReplyHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
