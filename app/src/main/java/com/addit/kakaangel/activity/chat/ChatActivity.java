package com.addit.kakaangel.activity.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.listner.RemoveUser;
import com.addit.kakaangel.listner.UpdateChatEvent;
import com.addit.kakaangel.model.ChatMessage;
import com.addit.kakaangel.model.ChatUser;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.codesgood.views.JustifiedTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ChatActivity extends BaseActivity implements RemoveUser {

    @BindView(R.id.img_back)
    ImageView mBack;
    @BindView(R.id.recycler_list)
    RecyclerView rvChat;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.mEmptyList)
    JustifiedTextView mEmptyList;

    Subscription subscriptionChatUser, subscriptionRemove;
    List<ChatUser> chatUsers = new ArrayList<>();
    ChatUserAdapter chatUserAdapter;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        ButterKnife.bind(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        tvTittle.setText("Messages");

        rvChat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//      linearLayoutManager.setReverseLayout(true);
        rvChat.setLayoutManager(linearLayoutManager);
        chatUserAdapter = new ChatUserAdapter(ChatActivity.this, chatUsers);
        rvChat.setAdapter(chatUserAdapter);
        getChatUsers(true);

        swipeLayout.setOnRefreshListener(() -> {
            if (L.isNetworkAvailable(ChatActivity.this)) {
                getChatUsers(false);
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateChatEvent(UpdateChatEvent event) {
        //getChatUsers(false);
        startActivity(new Intent(this, ChatMessageActivity.class)
                .putExtra(Constant.USERID, event.getChatUser()));
//        lastSelected = R.id.mMyPeep;
//        mMyPeep.setChecked(true);
//        navigateTo(new MyPeepTab(), null, false);
    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

        }
    }


    private void getChatUsers(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress( true);
        subscriptionChatUser = NetworkRequest.performAsyncRequest(restApi.getChatUsers(map), (data) -> {
            showProgress(false);
            swipeLayout.setRefreshing(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {
                        rvChat.setVisibility(View.VISIBLE);
                        mEmptyList.setVisibility(View.GONE);
                        chatUsers.clear();
                        chatUsers.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("chat_list").toString(), ChatUser.class));
                        chatUserAdapter.notifyDataSetChanged();
                    }else{
                        rvChat.setVisibility(View.GONE);
                        mEmptyList.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            swipeLayout.setRefreshing(false);
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        if (subscriptionChatUser != null && !subscriptionChatUser.isUnsubscribed()) {
            subscriptionChatUser.unsubscribe();
            subscriptionChatUser = null;
        }

        if (subscriptionRemove != null && !subscriptionRemove.isUnsubscribed()) {
            subscriptionRemove.unsubscribe();
            subscriptionRemove = null;
        }
        super.onDestroy();
    }

    @Override
    public void onRemoveUser(ChatUser chatUser, int pos) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.USERID, chatUser.getId());
        showProgress(true);
        subscriptionRemove = NetworkRequest.performAsyncRequest(restApi.deleteUser(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                chatUsers.remove(pos);
                chatUserAdapter.notifyDataSetChanged();
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }
}
