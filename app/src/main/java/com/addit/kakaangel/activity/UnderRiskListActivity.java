package com.addit.kakaangel.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.adapter.TradingListAdpter;
import com.addit.kakaangel.adapter.UnderRiskListAdpter;
import com.addit.kakaangel.model.TradingListModel;
import com.addit.kakaangel.model.UnderRiskListModel;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class UnderRiskListActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    Subscription subscription;
    UnderRiskListAdpter underRiskListAdpter;

    List<UnderRiskListModel> underRiskListModels = new ArrayList<>();
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trading_list);
        ButterKnife.bind(this);

        tvTittle.setText("Under Risk");

        if (L.isNetworkAvailable(this)) {
            UnderRiskList();
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        swipRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipRefresh.setRefreshing(false);
                        UnderRiskList();
                    }
                }
        );
    }
    private void UnderRiskList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ClientCode, user.getClientCode());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetunderiskList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        underRiskListModels.clear();
                        underRiskListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("underisk").toString(), UnderRiskListModel.class));
                        underRiskListAdpter=new UnderRiskListAdpter(this,underRiskListModels);
                        recyclerlist.setAdapter(underRiskListAdpter);
                    }else{
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}