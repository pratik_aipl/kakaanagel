package com.addit.kakaangel.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.activity.chat.ChatActivity;
import com.addit.kakaangel.activity.chat.ChatMessageActivity;
import com.addit.kakaangel.adapter.DashNotiListAdpter;
import com.addit.kakaangel.model.NotiListModel;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class DashboardActivity extends BaseActivity {

    private static final String TAG = "DashboardActivity";
    @BindView(R.id.img_noti)
    ImageView imgNoti;
    @BindView(R.id.tv_bal)
    TextView tvBal;
    @BindView(R.id.lin_trading)
    LinearLayout linTrading;
    @BindView(R.id.lin_security)
    LinearLayout linSecurity;
    @BindView(R.id.lin_fno)
    LinearLayout linFno;
    @BindView(R.id.lin_right_issue)
    LinearLayout linRightIssue;
    @BindView(R.id.lin_under_risk)
    LinearLayout linUnderRisk;
    @BindView(R.id.lin_chat)
    LinearLayout linChat;

    @BindView(R.id.fab_call)
    FloatingActionButton fabCall;

    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipRefresh;

    Subscription subscription;
    DashNotiListAdpter notiListAdpter;

    List<NotiListModel> notiListModels = new ArrayList<>();
    double NetLedgerValue = 0;
    @BindView(R.id.rel_header)
    RelativeLayout relHeader;
    @BindView(R.id.menu_item_one)
    com.github.clans.fab.FloatingActionButton menuItemOne;
    @BindView(R.id.menu_item_two)
    com.github.clans.fab.FloatingActionButton menuItemTwo;
    @BindView(R.id.menu_item_three)
    com.github.clans.fab.FloatingActionButton menuItemThree;
    @BindView(R.id.menu_item_four)
    com.github.clans.fab.FloatingActionButton menuItemFour;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rel_link)
    RelativeLayout relLink;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_count)
    TextView tv_count;
    @BindView(R.id.tv_clientID)
    TextView tv_clientID;

    MediaPlayer mPlayer;
    String currentVersion;

    @BindView(R.id.lin_anglTrading)
    LinearLayout linAnglTrading;
    @BindView(R.id.lin_backOfc)
    LinearLayout linBackOfc;

    private static boolean isFirstRun = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new GetVersionCode().execute();
        showProgress(true);

        tv_clientID.setText(user.getClientCode());

            if (L.isNetworkAvailable(this) ) {
                NOTIList();
            }


        if(isFirstRun) {
            if (L.isNetworkAvailable(this) ) {
                GetAdvertise();
                isFirstRun = false;
            }
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        swipRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipRefresh.setRefreshing(false);
                        NOTIList();
                    }
                }
        );


    }


    public void ShowDilaog(String URL){

        final Dialog dialog = new Dialog(this,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.zoom_pager_item);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView photoView = dialog.findViewById(R.id.img_zoom);
        ImageView mClose = dialog.findViewById(R.id.img_cancel);
        photoView.setVisibility(View.VISIBLE);
        if (URL != null)
            L.loadImageWithPicasso(this, URL, photoView,new ProgressBar(this,null,android.R.attr.progressBarStyleLarge));


        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void NOTIList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.TYPE, "TODAY");

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getNoti(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        relLink.setVisibility(View.VISIBLE);
                        notiListModels.clear();
                        notiListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("Notification").toString(), NotiListModel.class));
                        notiListAdpter = new DashNotiListAdpter(this, notiListModels);
                        recyclerlist.setAdapter(notiListAdpter);
                    } else {
                        relLink.setVisibility(View.GONE);
                    }

                    if (notiListModels.size() != 0) {
                        tv_count.setVisibility(View.VISIBLE);
                        tv_count.setText("" + notiListModels.size());

                        imgNoti.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bell_animation));
                        mPlayer = MediaPlayer.create(DashboardActivity.this, R.raw.bell_ring);
                        mPlayer.start();

                        int SPLASH_TIME_OUT = 3000;
                        new Handler(Looper.getMainLooper()).postDelayed(() -> {
                            imgNoti.clearAnimation();
                        }, SPLASH_TIME_OUT);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void GetAdvertise() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getAdvertisement(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "GetAdvertise: "+jsonResponse.toString());
                    String AdvURL=jsonResponse.getJSONObject("data").getJSONArray("Notification").getJSONObject(0).getString("ImageURL");
                    Log.d(TAG, "AdvURL: "+AdvURL);
                    ShowDilaog(AdvURL);

                  } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @OnClick({R.id.menu_item_one, R.id.menu_item_two, R.id.menu_item_three, R.id.menu_item_four, R.id.img_noti, R.id.img_logout, R.id.lin_trading, R.id.lin_security, R.id.lin_fno, R.id.lin_right_issue, R.id.lin_under_risk, R.id.lin_chat, R.id.lin_Knowledgebase, R.id.lin_anglTrading, R.id.lin_backOfc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_noti:
                startActivity(new Intent(DashboardActivity.this, NotificationListActivity.class));
                break;
            case R.id.img_logout:
                new AlertDialog.Builder(this)
                        .setMessage((R.string.logout))
                        .setPositiveButton("YES", (dialog, which) -> {
                            L.logout(this);
                        })
                        .setNegativeButton("NO", null)
                        .show();
                break;
            case R.id.lin_trading:
                startActivity(new Intent(DashboardActivity.this, TradingListActivity.class));
                break;
            case R.id.lin_security:
                startActivity(new Intent(DashboardActivity.this, SecurityHoldingListActivity.class));
                break;
            case R.id.lin_fno:
                startActivity(new Intent(DashboardActivity.this, FNOListActivity.class));
                break;
            case R.id.lin_right_issue:
                startActivity(new Intent(DashboardActivity.this, RightIssueListActivity.class));
                break;
            case R.id.lin_under_risk:
                startActivity(new Intent(DashboardActivity.this, UnderRiskListActivity.class));
                break;
            case R.id.lin_chat:
                /* 1=admin , 2= client */
                if (user.getRole_ID().equalsIgnoreCase("1")) {
                    startActivity(new Intent(DashboardActivity.this, ChatActivity.class));
                } else {
                    startActivity(new Intent(DashboardActivity.this, ChatMessageActivity.class)
                            .putExtra(Constant.USERID, "1")
                            .putExtra(Constant.USERNAME, "Admin"));
                }
                break;
            case R.id.lin_Knowledgebase:
                startActivity(new Intent(DashboardActivity.this, KnowledgeBaseListActivity.class));
                break;
            case R.id.lin_anglTrading:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.msf.angelmobile")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.msf.angelmobile&hl=en_IN&gl=US")));
                }
                break;

                case R.id.lin_backOfc:
                    Uri uri = Uri.parse("https://trade.angelbroking.com/");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);

                break;

            case R.id.menu_item_one:
                menu.close(true);
                Intent intent1 = new Intent(Intent.ACTION_DIAL);
                intent1.setData(Uri.parse("tel:9819282125"));
                startActivity(intent1);
                break;

            case R.id.menu_item_two:
                menu.close(true);
                Intent intent2 = new Intent(Intent.ACTION_DIAL);
                intent2.setData(Uri.parse("tel:7021417678"));
                startActivity(intent2);
                break;

            case R.id.menu_item_three:
                menu.close(true);
                Intent intent3 = new Intent(Intent.ACTION_DIAL);
                intent3.setData(Uri.parse("tel:7738043073"));
                startActivity(intent3);
                break;
            case R.id.menu_item_four:
                menu.close(true);
                Intent intent4 = new Intent(Intent.ACTION_DIAL);
                intent4.setData(Uri.parse("tel:9167260410"));
                startActivity(intent4);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (L.isNetworkAvailable(this)) {
            GetLendgerValue();
        }
    }

    private void GetLendgerValue() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ClientCode, user.getClientCode());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetLendgerValue(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        NetLedgerValue = Double.parseDouble(jsonResponse.getJSONObject(Constant.data).getJSONArray("csv").getJSONObject(0).getString("NetLedger"));
                        Log.d(TAG, "NetLedgerValue: " + NetLedgerValue);
                        tvBalance.setText("" + NetLedgerValue);
                        if (NetLedgerValue == 0) {
                            relHeader.setBackgroundColor(Color.parseColor("#B2000000"));
                        } else if (NetLedgerValue > 0) {
                            relHeader.setBackgroundColor(Color.parseColor("#cc589d0c"));
                        } else {
                            relHeader.setBackgroundColor(Color.parseColor("#ed2624"));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {


            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + DashboardActivity.this.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }

        }


        @Override

        protected void onPostExecute(String onlineVersion) {
            if (onlineVersion != null) {
                if (!currentVersion.equalsIgnoreCase(onlineVersion)) {
                    if (!isFinishing()) {
                        showUpdateDialog();
                    }
                }
            }
            super.onPostExecute(onlineVersion);
            showProgress(false);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.parseFloat(currentVersion) < Float.parseFloat(onlineVersion)) {
                    showUpdateDialog();
                }
            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }

    }

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("A New Update is Available");
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("market://details?id=" + DashboardActivity.this.getPackageName())));
                dialog.dismiss();
            }
        });


        builder.setCancelable(false);
        builder.show();
    }
}