package com.addit.kakaangel.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.adapter.SecurityHoldingListAdpter;
import com.addit.kakaangel.model.SecurityHoldingListModel;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SecurityHoldingListActivity extends BaseActivity {

    private static final String TAG = "SecurityHoldingListActi";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    Subscription subscription;
    SecurityHoldingListAdpter securityHoldingListAdpter;

    List<SecurityHoldingListModel> securityHoldingListModels = new ArrayList<>();
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipRefresh;
    @BindView(R.id.tv_holding_bal)
    TextView tvHoldingBal;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.lin_total)
    LinearLayout linTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trading_list);
        ButterKnife.bind(this);

        tvTittle.setText("Securities Holding");

        if (L.isNetworkAvailable(this)) {
            SecurityHoldingList();
        }

        linTotal.setVisibility(View.VISIBLE);

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        swipRefresh.setOnRefreshListener(
                () -> {
                    swipRefresh.setRefreshing(false);
                    SecurityHoldingList();
                }
        );

    }

    private void SecurityHoldingList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ClientCode, user.getClientCode());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetHoldingList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {
                        securityHoldingListModels.clear();
                        securityHoldingListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("holding").toString(), SecurityHoldingListModel.class));

                        securityHoldingListAdpter = new SecurityHoldingListAdpter(this, securityHoldingListModels);
                        recyclerlist.setAdapter(securityHoldingListAdpter);

                        double totalvalue = 0.0;
                        for (int i = 0; i < securityHoldingListModels.size(); i++) {
                            totalvalue = ((totalvalue)+Double.parseDouble(securityHoldingListModels.get(i).getNetValue()));
                        }
                        tvBalance.setText(""+new DecimalFormat("##.##").format(totalvalue));


                    } else {
                        Toast.makeText(this, "" + jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}