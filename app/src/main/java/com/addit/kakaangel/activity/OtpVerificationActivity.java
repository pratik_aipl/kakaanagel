package com.addit.kakaangel.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.BuildConfig;
import com.addit.kakaangel.R;
import com.addit.kakaangel.model.UserData;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class OtpVerificationActivity extends BaseActivity {

    private static final String TAG = "OtpVerificationActivity";
    @BindView(R.id.edt_otp)
    EditText edt_otp;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    Subscription subscription;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.tv_mob)
    TextView tvMob;
    @BindView(R.id.tv_resend)
    TextView tvResend;

    String MobileNo, OTP,CLIENTCODE;
    String PlayerID= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);
        ButterKnife.bind(this);

        MobileNo = getIntent().getExtras().getString(Constant.MOBILE);
        CLIENTCODE = getIntent().getExtras().getString(Constant.CLIENTCODE);
        OTP = getIntent().getExtras().getString(Constant.OTP);
        tvMob.setText(MobileNo);
        tvResend.setPaintFlags(tvResend.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        OneSignal.idsAvailable((userId, registrationId) -> {
            Log.d(TAG, "onCreate: User:" + userId);
            this.PlayerID = userId;
        });
        if(BuildConfig.DEBUG){
            edt_otp.setText("1234");
        }
    }
    @OnClick({R.id.img_back, R.id.btn_submit, R.id.tv_resend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_resend:
                GetOTP();
                break;
            case R.id.btn_submit:
                if (!L.getEditText(edt_otp).equalsIgnoreCase(OTP)) {
                    edt_otp.setError("Please enter Valid OTP");
                } else {
                    if (L.isNetworkAvailable(OtpVerificationActivity.this))
                        VerifyOTP();
                }
                break;
        }
    }


    private void VerifyOTP() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILE, MobileNo);
        map.put(Constant.CLIENTCODE, CLIENTCODE);
        map.put(Constant.OTP, L.getEditText(edt_otp));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.PlayerID, PlayerID);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.VerifyOTP(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken,jsonResponse.getString("login_token"));

                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void GetOTP() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.CLIENTCODE, CLIENTCODE);
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.PlayerID, PlayerID);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getOTP(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    OTP=DATA.getString("otp");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
