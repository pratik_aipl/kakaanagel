package com.addit.kakaangel.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.BaseActivity;
import com.addit.kakaangel.R;
import com.addit.kakaangel.adapter.RightIssueListAdpter;
import com.addit.kakaangel.adapter.SecurityHoldingListAdpter;
import com.addit.kakaangel.model.RightIssueListModel;
import com.addit.kakaangel.model.TradingListModel;
import com.addit.kakaangel.network.NetworkRequest;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class RightIssueListActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    Subscription subscription;
    RightIssueListAdpter rightIssueListAdpter;

    List<RightIssueListModel> rightIssueListModels = new ArrayList<>();
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trading_list);
        ButterKnife.bind(this);

        tvTittle.setText("IPO");

        if (L.isNetworkAvailable(this)) {
           RightIssueList();
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        swipRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipRefresh.setRefreshing(false);
                        RightIssueList();
                    }
                }
        );
    }
    private void RightIssueList() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ClientCode, user.getClientCode());

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetRightIssueList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        rightIssueListModels.clear();
                        rightIssueListModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("issue").toString(), RightIssueListModel.class));
                        rightIssueListAdpter=new RightIssueListAdpter(this,rightIssueListModels);
                        recyclerlist.setAdapter(rightIssueListAdpter);
                    }else{
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}