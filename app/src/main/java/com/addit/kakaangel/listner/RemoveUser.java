package com.addit.kakaangel.listner;


import com.addit.kakaangel.model.ChatUser;

public interface RemoveUser {
    public void onRemoveUser(ChatUser chatUser, int pos);
}
