package com.addit.kakaangel.listner;


import com.addit.kakaangel.model.ChatUser;

public class UpdateChatEvent {

    boolean isLoad;
    ChatUser chatUser;

    public UpdateChatEvent(boolean isLoad, ChatUser chatUser) {
        this.isLoad=isLoad;
        this.chatUser=chatUser;
    }

    public ChatUser getChatUser() {
        return chatUser;
    }

    public boolean isLoad() {
        return isLoad;
    }
}
