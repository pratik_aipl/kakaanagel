package com.addit.kakaangel.listner;


import com.addit.kakaangel.model.ChatMessage;

public interface MessageUsers {
    public void onUserClick(int pos, ChatMessage chatMessage);
}
