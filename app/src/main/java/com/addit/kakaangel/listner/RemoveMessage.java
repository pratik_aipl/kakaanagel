package com.addit.kakaangel.listner;


import com.addit.kakaangel.model.ChatMessage;

public interface RemoveMessage {
    public void onRemoveMsg(ChatMessage chatMessage, int pos);
}
