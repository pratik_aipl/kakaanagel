package com.addit.kakaangel.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TimeAgo {
    private static final String TAG = "TimeAgo";

    public static String getTimeAgo(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constant.DateFormat);
            Date past = gmtToLocalDate(format.parse(date));

            Log.d(TAG, "getTimeAgo: "+date);
            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());
            long weeks = days / 7;
            long month = weeks / 4;

            Log.d(TAG, "getTimeAgo: "+past);
            if (seconds < 60) {
                return seconds + " seconds ago";
            } else if (minutes < 60) {
                return minutes + " minutes ago";
            } else if (hours < 24) {
                return hours + " hours ago";
            } else if (days < 7) {
                return days + " days ago";
            } else if (weeks < 52) {
                return weeks + " week ago";
            } /*else if (month < 12) {
                return month + " month ago";
            } */else {
                return "years ago";
            }
        } catch (Exception j) {
            j.printStackTrace();
        }
        return "now";
    }

    public static Date gmtToLocalDate(Date date) {
        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));
        return local;
    }

}
