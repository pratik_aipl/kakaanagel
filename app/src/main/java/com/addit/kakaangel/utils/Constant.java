package com.addit.kakaangel.utils;

public class Constant {


    public static String file = "file";
    public static String DateFormat = "yyyy-MM-dd hh:mm:ss";

    public static String UserData = "UserData";
    public static String image = "image";
    public static String video = "video";
    public static String audio = "audio";
    public static String content = "content";
    public static String HTTP = "http";
    public static String HTTPS = "https";
    public static final String DeviceID = "device_token";
    public static boolean isAlertShow = false;
    public static String message = "message";
    public static String isLogin = "isLogin";
    public static String loginAuthToken = "loginAuthToken";
    public static String USERID = "UserId";
    public static String USERNAME = "username";
    public static String data = "data";
    public static String CATEGORYID = "CategoryID";
    public static String SUBCATEGORYID = "subcategoryid";
    public static String CATNAME = "catname";
    public static String PRODUCTLIST = "ProductList";
    public static String ORDERDETAILS = "orderdetails";
    public static String SUBTOTAL = "SUBTOTAL";
    public static String TotalPrice = "TotalPrice";
    public static String DCharge = "DCharge";
    public static final String PlayerID = "player_id";
    public static final String ClientCode = "ClientCode";


    public static String EMAILID = "email";
    public static String USERName = "username";
    public static String SURNAME = "surname";
    public static String MOBILE = "mobile_no";
    public static String CLIENTCODE = "ClientCode";
    public static String OTP = "otp";
    public static String TYPE = "TYPE";
    public static String Password = "password";
    public static String GENDER = "gender";
    public static String DOB = "dob";
    public static String ADDRESS = "address";
    public static String ZIPCODE = "zipcode";


    public static String cartData="cartData";
    public static String OrderId="OrderId";
}
