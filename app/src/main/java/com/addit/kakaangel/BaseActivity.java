package com.addit.kakaangel;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.addit.kakaangel.model.UserData;
import com.addit.kakaangel.network.RestAPIBuilder;
import com.addit.kakaangel.network.RestApi;
import com.addit.kakaangel.utils.Constant;
import com.addit.kakaangel.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onesignal.OneSignal;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.lang.reflect.Type;
import java.util.List;


public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    public Type type;

    ProgressDialog progressDialog;
    public RxPermissions rxPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }
    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
