package com.addit.kakaangel.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.KnowledgebaseListModel;
import com.addit.kakaangel.model.NotiListModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class KnowledgeBaseListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;

    List<KnowledgebaseListModel> singleitemList;


    public KnowledgeBaseListAdpter(Context context, List<KnowledgebaseListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationlistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        KnowledgebaseListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }

        holder.tvTittle.setText(listModel.getKb_titile());
        holder.tvDate.setText(listModel.getKb_date());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(listModel.getKd_link())){
                        if (!listModel.getKd_link().startsWith("http://") && !listModel.getKd_link().startsWith("https://")){
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+listModel.getKd_link()));
                            context.startActivity(browserIntent);
                        }else{
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(listModel.getKd_link()));
                            context.startActivity(browserIntent);
                        }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.tv_tittle)
        TextView tvTittle;
        @BindView(R.id.tv_date)
        TextView tvDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}