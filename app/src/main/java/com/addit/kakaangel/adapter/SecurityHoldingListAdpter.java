package com.addit.kakaangel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.SecurityHoldingListModel;
import com.addit.kakaangel.utils.L;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecurityHoldingListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
     List<SecurityHoldingListModel> singleitemList;

    public SecurityHoldingListAdpter(Context context, List<SecurityHoldingListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.securitylistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        SecurityHoldingListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }

        holder.tvScripCode.setText(listModel.getScripCode());
        holder.tvSymbol.setText(listModel.getSymbol());
        holder.tvNet.setText(listModel.getNET());
        holder.tvTotalvalue.setText(listModel.getNetValue());
        holder.tv_basis_total_value.setText("Closing Price : "+new DecimalFormat("##.##").format((Double.parseDouble(listModel.getNetValue())/Double.parseDouble(listModel.getNET()))));

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_scripCode)
        TextView tvScripCode;
        @BindView(R.id.tv_Symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_net)
        TextView tvNet;
        @BindView(R.id.tv_totalvalue)
        TextView tvTotalvalue;
        @BindView(R.id.tv_basis_total_value)
        TextView tv_basis_total_value;
        @BindView(R.id.card_view)
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}