package com.addit.kakaangel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.RightIssueListModel;
import com.addit.kakaangel.model.TradingListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RightIssueListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<RightIssueListModel> singleitemList;

    public RightIssueListAdpter(Context context, List<RightIssueListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rightissuelistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        RightIssueListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }


        holder.tvSymbol.setText(listModel.getSymbol());
        holder.tvBuyqty.setText(listModel.getQty());
        holder.tv_Client_code.setText(listModel.getClientCode());

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_Symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_Client_code)
        TextView tv_Client_code;
        @BindView(R.id.tv_buyqty)
        TextView tvBuyqty;
        @BindView(R.id.card_view)
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}