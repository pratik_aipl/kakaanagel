package com.addit.kakaangel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.FNOListModel;
import com.addit.kakaangel.model.SecurityHoldingListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FNOListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;

    List<FNOListModel> singleitemList;

    public FNOListAdpter(Context context, List<FNOListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }




    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fnolistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        FNOListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }

        holder.tvSegment.setText(listModel.getSegment());
        holder.tvSymbol.setText(listModel.getSymbol());
        holder.tvInstType.setText(listModel.getInstType());
        holder.tvExpDate.setText(listModel.getExpiryDate());
        holder.tvOptionType.setText(listModel.getOptionType());
        holder.tvStrikeprice.setText(listModel.getStrikePrice());
        holder.tvNetqty.setText(listModel.getNetQuantity());

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_Segment)
        TextView tvSegment;
        @BindView(R.id.tv_InstType)
        TextView tvInstType;
        @BindView(R.id.tv_Symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_expDate)
        TextView tvExpDate;
        @BindView(R.id.tv_OptionType)
        TextView tvOptionType;
        @BindView(R.id.tv_strikeprice)
        TextView tvStrikeprice;
        @BindView(R.id.tv_netqty)
        TextView tvNetqty;
        @BindView(R.id.card_view)
        CardView cardView;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}