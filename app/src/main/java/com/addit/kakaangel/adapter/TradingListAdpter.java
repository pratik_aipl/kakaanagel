package com.addit.kakaangel.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.TradingListModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TradingListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;

    List<TradingListModel> singleitemList;


    public TradingListAdpter(Context context, List<TradingListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tradinglistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        TradingListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }


        holder.tvDate.setText(listModel.getBuySellDate());
        holder.tvScriptCode.setText(listModel.getScripCode());
        holder.tvSymbol.setText(listModel.getSymbol());
        holder.tvBuyqty.setText(listModel.getBuyQty());
        holder.tvBuyavg.setText(listModel.getBuyPrice());
        holder.tvSellqty.setText(listModel.getSellQty());
        holder.tvSellavg.setText(listModel.getSellPrice());
        holder.tvMTMGL.setText(listModel.getStrike_Price());
        holder.tvMarketPrice.setText(listModel.getOption_Type());

        holder.tvSerexp.setText(listModel.getSer_Exp());

        int NetQTY = Integer.parseInt(listModel.getNetQty());
        holder.tvNetqty.setText("" + NetQTY);
        if (NetQTY == 0) {
            holder.tvNetqty.setTextColor(Color.parseColor("#000000"));
        } else if (NetQTY > 0) {
            holder.tvNetqty.setTextColor(Color.parseColor("#cc589d0c"));
        } else {
            holder.tvNetqty.setTextColor(Color.parseColor("#ed2624"));
        }
    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_serexp)
        TextView tvSerexp;
        @BindView(R.id.tv_ScriptCode)
        TextView tvScriptCode;
        @BindView(R.id.tv_Symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_Buyqty)
        TextView tvBuyqty;
        @BindView(R.id.tv_sellqty)
        TextView tvSellqty;
        @BindView(R.id.tv_Buyavg)
        TextView tvBuyavg;
        @BindView(R.id.tv_sellavg)
        TextView tvSellavg;
        @BindView(R.id.tv_netqty)
        TextView tvNetqty;
        @BindView(R.id.tv_MTMGL)
        TextView tvMTMGL;
        @BindView(R.id.tv_MarketPrice)
        TextView tvMarketPrice;
        @BindView(R.id.card_view)
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}