package com.addit.kakaangel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.TradingListModel;
import com.addit.kakaangel.model.UnderRiskListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnderRiskListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;

    List<UnderRiskListModel> singleitemList;

    public UnderRiskListAdpter(Context context, List<UnderRiskListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.underrisklistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        UnderRiskListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }

        //holder.tvSegment.setText(listModel.get());
        holder.tvLbal.setText(listModel.getLedgerBalance());
        holder.tvShortage.setText(listModel.getShortage());
        holder.tvSquareOffValue.setText(listModel.getSquareoffValue());
        holder.tvReason.setText(listModel.getReason());
        holder.tv_SquareOffDate.setText(listModel.getSquareoffDate());

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_Segment)
        TextView tvSegment;
        @BindView(R.id.tv_Lbal)
        TextView tvLbal;
        @BindView(R.id.tv_Shortage)
        TextView tvShortage;
        @BindView(R.id.tv_SquareOffValue)
        TextView tvSquareOffValue;
        @BindView(R.id.tv_Reason)
        TextView tvReason;
        @BindView(R.id.tv_SquareOffDate)
        TextView tv_SquareOffDate;
        @BindView(R.id.card_view)
        CardView cardView;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}