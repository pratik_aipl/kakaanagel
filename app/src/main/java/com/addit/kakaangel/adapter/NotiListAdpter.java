package com.addit.kakaangel.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.addit.kakaangel.R;
import com.addit.kakaangel.model.NotiListModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotiListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;

    List<NotiListModel> singleitemList;

    public NotiListAdpter(Context context, List<NotiListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationlistrow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        NotiListModel listModel = singleitemList.get(position);

        if (position % 2 == 0) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.Gray));

        }


        if(!TextUtils.isEmpty(listModel.getImageURL())){
            holder.imgNoti.setVisibility(View.VISIBLE);
            Picasso.get().load(listModel.getImageURL()).error(R.mipmap.logo_round).placeholder(R.mipmap.logo_round).into(holder.imgNoti);
        }
        holder.tvTittle.setText(listModel.getTitle());
        holder.tvDate.setText(listModel.getDate());

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_noti)
        ImageView imgNoti;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.tv_tittle)
        TextView tvTittle;
        @BindView(R.id.tv_date)
        TextView tvDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}